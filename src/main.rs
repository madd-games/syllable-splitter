use std::{fs, env, process::exit};

use dict::Dict;
use html_editor::operation::Htmlifiable;

use crate::transform::TransformSyllables;

mod dict;
mod transform;

#[macro_use]
extern crate serde_derive;

fn main() {
    let args: Vec<String> = env::args().collect();
    let input_file = match args.get(1) {
        Some(name) => name.clone(),
        None => {
            eprintln!("SYNTAX: {} input_file [output_file]", args[0]);
            exit(1);
        },
    };

    let output_file = match args.get(2) {
        Some(name) => name.clone(),
        None => format!("{input_file}.out"),
    };

    let mut dict = Dict::load();
    
    let data = fs::read_to_string(&input_file).unwrap();
    let mut dom = html_editor::parse(&data).unwrap();

    dom.transform_syllables(&mut dict);

    let html = dom.html();
    fs::write(&output_file, &html).unwrap();
}
