use std::{collections::HashMap, fs, io, path::PathBuf};

/// Definition of a word in the dictionary.
#[derive(Clone, Serialize, Deserialize)]
pub struct WordDefinition {
    syllables: Vec<String>,
}

/// The dictionary of words.
#[derive(Clone, Serialize, Deserialize)]
pub struct Dict {
    words: HashMap<String, WordDefinition>,
}

impl WordDefinition {
    pub fn output_syllables(&self, output: &mut String) {
        let joined = self.syllables.join("&shy;");
        output.push_str(&joined);
    }
}

impl Dict {
    fn get_dict_path() -> PathBuf {
        let mut result = dirs2::home_dir().unwrap();
        result.push("syllable-dict.json");
        result
    }

    /// Creates a new dictionary.
    pub fn new() -> Self {
        Self {
            words: HashMap::new(),
        }
    }

    /// Load from a file.
    pub fn load() -> Self {
        match fs::read_to_string(&Self::get_dict_path()) {
            Ok(data) => serde_json::from_str(&data).unwrap(),
            Err(_) => Self::new(),
        }
    }

    /// Save to the file.
    fn save(&self) {
        fs::write(&Self::get_dict_path(), serde_json::to_string_pretty(self).unwrap()).unwrap();
    }

    /// Query a word, potentially asking the user.
    pub fn query(&mut self, word: &str) -> &WordDefinition {
        if !self.words.contains_key(word) {
            println!("Unknown word: '{}'; enter hyphenation (e.g. co-co-nut):", word);

            let syllables = loop {
                let line = io::stdin().lines().next().unwrap().unwrap();

                let mut syllables = Vec::<String>::new();
                let mut recombined = String::new();

                for token in line.split("-") {
                    syllables.push(token.to_owned());
                    recombined.push_str(token);
                }

                if recombined == word {
                    break syllables;
                }

                println!("The word does not recombine correctly ({}). Try again.", recombined);
                println!();
            };

            self.words.insert(word.to_owned(), WordDefinition {syllables});
            self.save();
        }

        &self.words[word]
    }
}