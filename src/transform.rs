use html_editor::Node;

use crate::dict::Dict;

pub trait TransformSyllables {
    fn transform_syllables(&mut self, dict: &mut Dict);
}

impl TransformSyllables for Vec<Node> {
    fn transform_syllables(&mut self, dict: &mut Dict) {
        for node in self {
            node.transform_syllables(dict);
        }
    }
}

impl TransformSyllables for Node {
    fn transform_syllables(&mut self, dict: &mut Dict) {
        match self {
            Node::Text(text) => text.transform_syllables(dict),
            Node::Element(el) => el.children.transform_syllables(dict),
            _ => {},
        }
    }
}

impl TransformSyllables for String {
    fn transform_syllables(&mut self, dict: &mut Dict) {
        if self.trim().is_empty() {
            return;
        }

        let mut updated = String::new();

        let mut next_word = String::new();

        for c in self.chars() {
            if c.is_alphabetic() {
                next_word.push(c);
            }
            else if next_word.len() == 1 && c == ' ' {
                updated.push_str(&next_word);
                updated.push_str("&nbsp;");
                next_word.clear();
            }
            else {
                process_word(&next_word, dict, &mut updated);
                next_word.clear();
                updated.push(c);
            }
        }

        process_word(&next_word, dict, &mut updated);

        self.clear();
        self.push_str(&updated);
    }
}

fn process_word(word: &str, dict: &mut Dict, output: &mut String) {
    if word.len() == 1 {
        output.push_str(&word);
        return;
    }

    if word.is_empty() {
        return;
    }

    let wdef = dict.query(word);
    wdef.output_syllables(output);
}